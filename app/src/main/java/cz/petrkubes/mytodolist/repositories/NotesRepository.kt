package cz.petrkubes.mytodolist.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.petrkubes.mytodolist.data.Note
import cz.petrkubes.mytodolist.room.NotesDatabase
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class NotesRepository @Inject constructor(
    private val db: NotesDatabase
) {
    private val allNotes: LiveData<List<Note>> = db.noteDao().getAllNotes()

    fun insertNote(note: Note) {
        db.noteDao().insertNote(note)
    }

    fun deleteNote(note: Note) {
        db.noteDao().deleteNote(note)
    }

    fun deleteAllNotes() {
        db.noteDao().deleteAllNotes()
    }

    fun getNotes(): LiveData<List<Note>> {
        return allNotes
    }
}