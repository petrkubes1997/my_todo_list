package cz.petrkubes.mytodolist.dagger

import cz.petrkubes.mytodolist.services.NotificationsService
import cz.petrkubes.mytodolist.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, ViewModelModule::class])
@Singleton
interface AppComponent {
    fun inject(activity: MainActivity)

    fun inject(notificationsService: NotificationsService)
}