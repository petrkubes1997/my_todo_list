package cz.petrkubes.mytodolist.dagger

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cz.petrkubes.mytodolist.room.NotesDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    private val db: NotesDatabase = Room.databaseBuilder(
       context,
       NotesDatabase::class.java,
        "todo-db"
   ).build()


    @Provides
    fun provideDatabase(): NotesDatabase {
        return db
    }
}