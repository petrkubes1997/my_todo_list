package cz.petrkubes.mytodolist.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.petrkubes.mytodolist.R
import cz.petrkubes.mytodolist.data.Note
import cz.petrkubes.mytodolist.ui.MainActivity
import kotlinx.android.synthetic.main.dialog_add_note.*
import kotlinx.android.synthetic.main.dialog_add_note.view.*

@DialogColor(android.R.color.holo_red_light)
class AddNoteDialog: ColorfulDialog() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_add_note, container, false)
        val viewmodel = (activity as MainActivity).viewModel

        view.btn_save.setOnClickListener {
            viewmodel.insertNote(Note(et_title.text.toString(), et_content.text.toString())) { this.dismiss() }
            btn_save.isEnabled = false
        }

        view.btn_cancel.setOnClickListener {
            this.dismiss()
        }

        return view
    }
}