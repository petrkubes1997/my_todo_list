package cz.petrkubes.mytodolist.ui.dialogs

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment

abstract class ColorfulDialog : DialogFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        context?.let {
            val dialogColor = javaClass.getAnnotation(DialogColor::class.java)
            dialogColor?.let {
                view.setBackgroundColor(ContextCompat.getColor(context!!, it.color))
            }
        }
        super.onViewCreated(view, savedInstanceState)

    }
}