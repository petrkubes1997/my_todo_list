package cz.petrkubes.mytodolist.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import cz.petrkubes.mytodolist.App
import cz.petrkubes.mytodolist.R
import cz.petrkubes.mytodolist.data.Note
import cz.petrkubes.mytodolist.ui.dialogs.AddNoteDialog
import cz.petrkubes.mytodolist.viewmodels.NotesViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    // Public so that it can be shared with fragment dialog
    lateinit var viewModel: NotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(cz.petrkubes.mytodolist.R.layout.activity_main)
        (application as App).appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[NotesViewModel::class.java]

        val viewManager = LinearLayoutManager(this)
        val viewAdapter = ListAdapter(emptyList())

        rc_list.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        viewModel.getAllNotes().observe(this, Observer { notes: List<Note> ->
            viewAdapter.setData(notes)
        })

        fab_add_note.setOnClickListener {
            showAddNoteDialog()
        }

        // Handle notification
        val extras = intent.extras
        val title = extras?.getString("title")
        val description = extras?.getString("desc")
        if (title != null && description != null) {
            viewModel.insertNote(Note(title, description)) {}
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.mi_delete_all -> {
                viewModel.deleteAllNotes {  }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showAddNoteDialog() {
        val addNoteDialog = AddNoteDialog()
        addNoteDialog.show(supportFragmentManager, "dialog")
    }
}
