package cz.petrkubes.mytodolist.ui.dialogs

import androidx.annotation.ColorRes
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
annotation class DialogColor(@ColorRes val color: Int)
