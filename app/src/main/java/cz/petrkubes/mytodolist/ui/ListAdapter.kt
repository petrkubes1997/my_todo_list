package cz.petrkubes.mytodolist.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import cz.petrkubes.mytodolist.R
import cz.petrkubes.mytodolist.data.Note
import kotlinx.android.synthetic.main.list_item.view.*

class ListAdapter(private var notes: List<Note>) : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    class MyViewHolder(val llItem: ConstraintLayout) : RecyclerView.ViewHolder(llItem)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapter.MyViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false) as ConstraintLayout
        return MyViewHolder(root)
    }

    fun setData(notes: List<Note>) {
        this.notes = notes
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.llItem.tv_title.text = notes[position].title
        holder.llItem.tv_content.text = notes[position].content
    }

    override fun getItemCount() = notes.size
}