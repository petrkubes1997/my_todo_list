package cz.petrkubes.mytodolist.services

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import cz.petrkubes.mytodolist.App
import cz.petrkubes.mytodolist.data.Note
import cz.petrkubes.mytodolist.repositories.NotesRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject


class NotificationsService : FirebaseMessagingService() {

    @Inject
    lateinit var notesRepository: NotesRepository

    override fun onCreate() {
        super.onCreate()
        (application as App).appComponent.inject(this)
    }

    override fun onNewToken(token: String?) {
        Log.d("MY_TAG", "Refreshed token: $token")
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Log.d("asdf", "From: " + remoteMessage!!.from!!)

        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            Log.d("asdf", "Message data payload: " + remoteMessage.data)
            GlobalScope.launch {
                notesRepository.insertNote(Note(
                    remoteMessage.data["title"].toString(),
                    remoteMessage.data["desc"].toString()
                ))
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d("asdf", "Message Notification Body: " + remoteMessage.notification!!.body!!)
        }
    }
}