package cz.petrkubes.mytodolist.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.petrkubes.mytodolist.data.Note
import cz.petrkubes.mytodolist.repositories.NotesRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class NotesViewModel @Inject constructor(private val notesRepository: NotesRepository) : ViewModel() {

    fun getAllNotes(): LiveData<List<Note>> {
        return notesRepository.getNotes()
    }

    fun insertNote(note: Note, cb: (Boolean) -> Unit) {
        GlobalScope.launch {
            notesRepository.insertNote(note)
            cb(true)
        }
    }

    fun deleteNote(note: Note, cb: (Boolean) -> Unit) {
        GlobalScope.launch {
            notesRepository.deleteNote(note)
            cb(true)
        }
    }

    fun deleteAllNotes(cb: (Boolean) -> Unit) {
        GlobalScope.launch {
            notesRepository.deleteAllNotes()
            cb(true)
        }
    }

}