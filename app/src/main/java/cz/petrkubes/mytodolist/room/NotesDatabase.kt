package cz.petrkubes.mytodolist.room

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.petrkubes.mytodolist.data.Note

@Database(entities = [Note::class], version = 1)
abstract class NotesDatabase : RoomDatabase() {
    abstract fun noteDao(): NotesDao
}