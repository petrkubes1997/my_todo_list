package cz.petrkubes.mytodolist.ui

import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import cz.petrkubes.mytodolist.R
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy


@RunWith(AndroidJUnit4::class)
@LargeTest
class Screenshots {

    private lateinit var stringToBetyped: String

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java)

    @Before
    fun init() {
        Screengrab.setDefaultScreenshotStrategy(UiAutomatorScreenshotStrategy())
    }

    @Test
    fun addNotes() {
        val title = "Test title"
        val content = "Test content"

        onView(withId(R.id.mi_delete_all))
            .perform(click())

        onView(withId(R.id.fab_add_note))
            .perform(click())

        Screengrab.screenshot("add_note");

        onView(withId(R.id.et_title))
            .perform(typeText(title))
        onView(withId(R.id.et_content))
            .perform(typeText(content))
        closeSoftKeyboard()
        onView(withId(R.id.btn_save))
            .perform(click())
        closeSoftKeyboard()

        Thread.sleep(100)

        Screengrab.screenshot("view_notes");

        onView(withText(title)).check { _, _ -> isDisplayed() }
    }
}
